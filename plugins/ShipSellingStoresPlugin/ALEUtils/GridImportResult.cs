﻿/**
 * Code taken directly from the ALE Core: https://github.com/LordTylus/SE-Torch-ALE-Core
 * Code originally written by Lord Tylus and licensed under Apache 2.0 which is included in this repo
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipStorePlugin.ALEUtils {
    public enum GridImportResult {
        OK,
        NO_GRIDS_IN_FILE,
        UNKNOWN_ERROR,
        NO_GRIDS_IN_BLUEPRINT,
        NO_FREE_SPACE_AVAILABLE,
        NOT_COMPATIBLE,
        POTENTIAL_BLOCKED_SPACE,
        FILE_NOT_FOUND,
    }
}
