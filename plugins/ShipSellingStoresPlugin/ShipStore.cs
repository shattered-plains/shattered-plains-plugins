﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using Torch;
using Torch.API;
using Torch.API.Session;
using Torch.API.Managers;
using Torch.Session;

using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;

namespace ShipStorePlugin {
    public class ShipStore : TorchPluginBase {

        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private TorchSessionManager _sessionManager;

        private string ShipStoreConfigFileName = "ShipStoreConfig.cfg";

        private Persistent<Config.ShipStoreConfig> _config;
        public Config.ShipStoreConfig Config => _config?.Data;

        /// <inheritdoc />
        public override void Init(ITorchBase torch) {
            base.Init(torch);

            // Register method 'SessionChanged' to SessionStateChanged action
            _sessionManager = Torch.Managers.GetManager<TorchSessionManager>();
            if (_sessionManager != null) {
                _sessionManager.SessionStateChanged += SessionChanged; // Register function to event
            } else {
                Log.Warn("No session manager loaded");
            }

            // Setup Config file
            SetupConfig();


            Log.Info("Initializing Ship Store Plugin!!");
        }

        private void SetupConfig() {

            var configFile = Path.Combine(StoragePath, ShipStoreConfigFileName);

            try {

                _config = Persistent<Config.ShipStoreConfig>.Load(configFile);

            } catch (Exception e) {
                Log.Warn(e);
            }

            if (_config?.Data == null) {

                Log.Info("Create Default Config, because none was found!");

                _config = new Persistent<Config.ShipStoreConfig>(configFile, new Config.ShipStoreConfig());
                _config.Save();
            }
        }

        public void SaveConfig() {
            _config.Save();
        }

        /// <summary>
        /// Called every time the session state is changed (ie. the session is now unloading)
        /// </summary>
        private void SessionChanged(ITorchSession _session, TorchSessionState _state) {
            switch (_state) {
                case TorchSessionState.Loaded:
                    Log.Info("Torch Session Loaded");

                    break;
                case TorchSessionState.Unloading:
                    Log.Info("Torch Session Unloading");

                    SaveConfig();
                    break;
            }
        }

        /// <summary>
        /// Function from ALE Grid Exporter - used to construct filepath for gridnames that are exported.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string CreatePath(string fileName) {
            var folder = Path.Combine(StoragePath, Config.FileImportFolderName);
            Directory.CreateDirectory(folder);
            return Path.Combine(folder, fileName + ".sbc");
        }
    }
}
