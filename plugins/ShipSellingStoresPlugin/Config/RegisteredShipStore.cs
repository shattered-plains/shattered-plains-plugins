﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using VRage;
using VRage.Game.ModAPI;
using VRageMath;

namespace ShipStorePlugin.Config {
    public class RegisteredShipStore : ViewModel {

        public RegisteredShipStore() {
            ShipsPresentToSell.CollectionChanged += (sender, args) => OnPropertyChanged();
        }

        /// <summary>
        /// The name of the ship store
        /// </summary>
        private string _name;
        public string Name { get { return _name; } set { _name = value; } }

        /// <summary>
        /// The faction tag (ie: SPRT) of the faction that owns this ship store. 
        /// A null value or invalid faction represents an admin ship store that doesn't need to worry about funds
        /// </summary>
        private string _ownerFactionTag;
        public string OwnerFactionTag { get { return _ownerFactionTag; } set { _ownerFactionTag = value; } }

        /// <summary>
        /// Location of the ship store
        /// </summary>
        private double _x;
        private double _y;
        private double _z;
        public double X { get { return _x; } set { _x = value; } }
        public double Y { get { return _y; } set { _y = value; } }
        public double Z { get { return _z; } set { _z = value; } }

        /// <summary>
        /// The radius of the ship store
        /// </summary>
        private double _radius;
        public double Radius { get { return _radius; } set { _radius = value; } }


        /// <summary>
        /// List of ships availible at the current location
        /// </summary>
        public ObservableCollection<RegisteredShipPresentToSell> ShipsPresentToSell { get; } = new ObservableCollection<RegisteredShipPresentToSell>();
        public class RegisteredShipPresentToSell : ViewModel {

            /// <summary>
            /// Grid Name of the ship being sold
            /// </summary>
            private string _shipName;
            public string ShipName { get { return _shipName; } set { _shipName = value; } }

            /// <summary>
            /// Id of the ship being sold
            /// </summary>
            private long _shipId;
            public long ShipID { get { return _shipId; } set { _shipId = value; } }

            /// <summary>
            /// Cost in credits to buy the ship
            /// </summary>
            private long _shipCost;
            public long ShipCost { get { return _shipCost; } set { _shipCost = value; } }

            public override string ToString() {
                return $"{this._shipName}";
            }
        }



        /// <summary>
        /// List of ships that can be bought at the ship store but need to be spawned in
        /// </summary>
        public ObservableCollection<RegisteredShipModelToSell> ShipModelsToSell { get; } = new ObservableCollection<RegisteredShipModelToSell>();
        public class RegisteredShipModelToSell : ViewModel {
            /// <summary>
            /// Blueprint name of the ship being sold
            /// </summary>
            private string _shipName;
            public string ShipName { get { return _shipName; } set { _shipName = value; } }

            /// <summary>
            /// Cost in credits to buy the ship
            /// </summary>
            private long _shipCost;
            public long ShipCost { get { return _shipCost; } set { _shipCost = value; } }

            public override string ToString() {
                return $"{this._shipName}";
            }
        }

        public static RegisteredShipStore TryGetShipStoreFromLocation(Vector3D _location, ObservableCollection<RegisteredShipStore> _stores) {
            foreach (RegisteredShipStore store in _stores) {
                // Check if location is in store radius
                if (Vector3D.Distance(_location, new Vector3D(store.X, store.Y, store.Z)) <= store.Radius) {
                    return store;
                }
            }

            // Could not find a store at this location, return a null value qwq
            return null;
        }

        /// <summary>
        /// Check if a point with radius will overlap with another ship store (Used to prevent ship stores from overlapping)
        /// </summary>
        /// <param name="_location"></param>
        /// <param name="_proposedRadius"></param>
        /// <param name="_stores"></param>
        /// <returns></returns>
        public static RegisteredShipStore TryGetShipStoreOverlap(Vector3D _location, double _proposedRadius, ObservableCollection<RegisteredShipStore> _stores) {
            foreach (RegisteredShipStore store in _stores) {
                // Check if location is in store radius
                if (Vector3D.Distance(_location, new Vector3D(store.X, store.Y, store.Z)) <= (store.Radius+ _proposedRadius)) {
                    return store;
                }
            }

            // Could not find a store at this location, return a null value qwq
            return null;
        }

        public override string ToString() {
            return $"{this._name}";
        }
    }
}
