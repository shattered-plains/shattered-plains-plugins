﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using Torch.Views;
using System.Xml.Serialization;

namespace ShipStorePlugin.Config {
    public class ShipStoreConfig : ViewModel{

        public ShipStoreConfig() {
            RegisteredShipStores.CollectionChanged += (sender, args) => OnPropertyChanged();
        }

        public ObservableCollection<RegisteredShipStore> RegisteredShipStores { get; } = new ObservableCollection<RegisteredShipStore>();
        
        /// <summary>
        /// Toggles if Players can Create Ship Stores themselfs (grants them access)
        /// </summary>
        private bool _playersCanCreateShipStores = true;
        public bool PlayersCanCreateShipStores {
            get => _playersCanCreateShipStores;
            set => SetValue(ref _playersCanCreateShipStores, value);
        }

        /// <summary>
        /// What the ALE grid export folder is called
        /// </summary>
        private string _fileImportFolderName = "ExportedGrids";
        public string FileImportFolderName {
            get => _fileImportFolderName;
            set => SetValue(ref _fileImportFolderName, value);
        }

        /// <summary>
        /// The maximum radius a ship store can be
        /// </summary>
        private double _maxShipStoreRadius = 250;
        public double MaxShipStoreRadius {
            get => _maxShipStoreRadius;
            set => SetValue(ref _maxShipStoreRadius, value);
        }

    }
}
