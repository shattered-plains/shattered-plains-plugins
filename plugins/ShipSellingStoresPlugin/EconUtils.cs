﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VRage.Game.VisualScripting;
using Sandbox;
using VRage;
using Sandbox.ModAPI;
using VRage.Game.ModAPI;
using Sandbox.Game.World;


namespace ShipStorePlugin {
    public class EconUtils {

        /// <summary>
        /// Attempts to change an account associated with a playerID by a specified amount
        /// </summary>
        public static void ChangePlayerCreditAccount(long _playerId, long _amount) {
            // Implement a small try catch in-case this wants to throw a "can't find player exception"
            try {
                MyAPIGateway.Multiplayer.Players.RequestChangeBalance(_playerId, _amount);
            } catch (Exception e) {
                ShipStore.Log.Warn(e.Message);
            }
        }

        /// <summary>
        /// Tries to get the balance of a player by their player id. Returns a 0 balance if this fails
        /// </summary>
        public static long TryGetPlayerAccountBalance(long _playerId) {
            // Implement a small try catch in-case this wants to throw a "can't find player exception"
            try {
                List<IMyPlayer> _players = new List<IMyPlayer>();
                MyAPIGateway.Multiplayer.Players.GetPlayers(_players); // Get all players

                // Get player with the specified player id
                IMyPlayer player = null;
                foreach (IMyPlayer _player in _players) {
                    if (_player.IdentityId == _playerId) {
                        player = _player;
                    }
                }

                // Could not find the player - return 0
                if (player == null) {
                    return 0;
                }

                long balance;
                player.TryGetBalanceInfo(out balance);
                return balance;

            } catch (Exception e) {
                ShipStore.Log.Warn(e.Message);
                return 0;
            }
        }


        /// <summary>
        /// Attempts to change an account associated with a faction by a specified amount
        /// </summary>
        public static void ChangeFactionCreditAccount(string _factionTag, long _amount) {
            // Implement a small try catch in-case this wants to throw a "can't find faction exception"
            try {

                IMyFaction _faction = MySession.Static.Factions.TryGetFactionByTag(_factionTag) as IMyFaction;
                _faction.RequestChangeBalance(_amount);

            } catch (Exception e) {
                ShipStore.Log.Warn(e);
            }
        }

        /// <summary>
        /// Tries to get the balance of a faction by their faction tag. Returns a 0 balance if this fails
        /// </summary>
        /// <param name="_factionTag"></param>
        /// <returns></returns>
        public static long TryGetFactionAccountBalance(string _factionTag) {
            // Implement a small try catch in-case this wants to throw a "can't find faction exception"
            try {
                long _amount;
                IMyFaction _faction = MySession.Static.Factions.TryGetFactionByTag(_factionTag) as IMyFaction;
                _faction.TryGetBalanceInfo(out _amount);
                return _amount;

            } catch (Exception e) {
                ShipStore.Log.Warn(e);
                return 0;
            }
        }

    }
}
