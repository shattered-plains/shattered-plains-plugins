﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRage.Game.Entity;
using Sandbox.Game;
using Sandbox.Game.World;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;

using NLog;
using System.IO;
using System.Windows;
using Torch;
using Torch.API;
using Torch.API.Plugins;
using VRageMath;


namespace ShipStorePlugin {
    public class ShipStoreUtils {
        public static void ClaimGrids(List<MyCubeGrid> _grids, long _playerId) {
            foreach (MyCubeGrid _grid in _grids) {
                _grid.ChangeGridOwner(_playerId, VRage.Game.MyOwnershipShareModeEnum.Faction);
            }
        }
    }
}
