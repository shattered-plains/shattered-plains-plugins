﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRage.Game.Entity;
using Sandbox.Game;
using Sandbox.Game.World;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;

using NLog;
using System.IO;
using System.Windows;
using Torch;
using Torch.API;
using Torch.API.Plugins;
using VRageMath;

namespace ShipStorePlugin {

    [Category("sstore")]
    public class ShipStoreCommands : CommandModule {

        // Get our current plugin instance from Context.Plugin
        public ShipStore Plugin => (ShipStore)Context.Plugin;

        [Command("createshipstore", "Creates a ship store with a name and optional faction tag")]
        [Permission(MyPromoteLevel.None)]
        public void CreateShipStore (string _storeName, string _factionTag = null) {
            // Stop here if players cannot create ship stores and caller is not a space master or better
            if (!Plugin.Config.PlayersCanCreateShipStores) {
                if (Context.Player.PromoteLevel < MyPromoteLevel.SpaceMaster) {
                    Context.Respond("Cannot create new ship store. You need Spacemaster or better privileges");
                    return;
                }
            }

            Config.RegisteredShipStore newShipStore = new Config.RegisteredShipStore();
            newShipStore.Name = _storeName;

            // Add location and radius
            Vector3D location = new Vector3D();
            if (Context.Player != null) location = Context.Player.GetPosition();
            newShipStore.X = location.X;
            newShipStore.Y = location.Y;
            newShipStore.Z = location.Z;

            newShipStore.Radius = Plugin.Config.MaxShipStoreRadius;

            // Make sure we won't create an overlap between two ship stores by creating a new one
            Config.RegisteredShipStore overlappingStore = Config.RegisteredShipStore.TryGetShipStoreOverlap(location, newShipStore.Radius, Plugin.Config.RegisteredShipStores);
            if (overlappingStore != null) {
                Context.Respond($"Error: Proposed new ship store overlaps with store with name: {overlappingStore.Name}");
                return;
            }

            // Add owner faction tag (if availible)
            if (_factionTag != null) {
                // Make sure the faction exists
                if (MyVisualScriptLogicProvider.GetFactionMembers(_factionTag).Count == 0) {
                    Context.Respond($"Sorry, {_factionTag} does not exists");
                    return;
                }

                newShipStore.OwnerFactionTag = _factionTag;
            }
            else if (Context.Player != null) {
                newShipStore.OwnerFactionTag = MyVisualScriptLogicProvider.GetPlayersFactionTag(Context.Player.IdentityId);
            }

            // Looks like everything is good!
            Context.Respond($"Creating {newShipStore.Name} at {location.ToString("0.00")} for faction with tag: {newShipStore.OwnerFactionTag}");

            // Add to list of registered ship stores
            Plugin.Config.RegisteredShipStores.Add(newShipStore);
        }

        [Command("sellshipblueprint", "Setup either the ship store your character is present in, or the store by name to sell a ship blueprint.")]
        [Permission(MyPromoteLevel.SpaceMaster)]
        public void SellShipBlueprint(string _shipBp, long _cost, string _storename = null) {
            Config.RegisteredShipStore store = null;

            // Get the current store since no store name was specified
            if (_storename == null) {
                if (Context.Player == null) {
                    Context.Respond("Could not find ship store - No player to get location from");
                    return;
                }

                store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);

                if (store == null) {
                    Context.Respond("Could not find a ship store at your present location");
                    return;
                }
            }

            // Try to get the store with the specified name (case insensitive)
            else {
                foreach (Config.RegisteredShipStore _store in Plugin.Config.RegisteredShipStores) {
                    if (_store.Name.ToLower() == _storename.ToLower()) {
                        store = _store;
                        break;
                    }
                }

                if (store == null) {
                    Context.Respond($"Could not find a ship store with the name {_storename}");
                    return;
                }
            }

            // Check if the ship bp exists
            if (!File.Exists(Plugin.CreatePath(_shipBp))) {
                Context.Respond($"Could not find a ship bp with name {_shipBp}");
                return;
            }

            // Looks like everything is in order!
            Config.RegisteredShipStore.RegisteredShipModelToSell newShipModel = new Config.RegisteredShipStore.RegisteredShipModelToSell();
            newShipModel.ShipName = _shipBp;
            newShipModel.ShipCost = _cost;

            // Add new ship model to ship store
            Context.Respond($"Adding {_shipBp} to {store.Name} with price of {_cost} credits");
            store.ShipModelsToSell.Add(newShipModel);
        }

        [Command("sellship", "Setup the grid you are looking at and all its subgrids to be sold for [cost] credits")]
        [Permission(MyPromoteLevel.None)]
        public void SellShipPresent(long _cost) {
            // Must be executed by a player
            if (Context.Player == null) {
                Context.Respond("Cannot run this command as the server");
                return;
            }

            // Must be executed by a character
            var player = ((MyPlayer)Context.Player).Identity;

            if (player.Character == null) {
                Context.Respond("Player has no character");
                return;
            }

            // Get the store from your current location
            Config.RegisteredShipStore store;
            store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);
            if (store == null) {
                Context.Respond("Could not find a ship store at your present location");
                return;
            }

            // Make sure the Player calling this is in the same faction as the ship store or has space master permissions
            string playerFactionTag = MyVisualScriptLogicProvider.GetPlayersFactionTag(Context.Player.IdentityId);
            if (playerFactionTag != store.OwnerFactionTag) {
                if (Context.Player.PromoteLevel < MyPromoteLevel.SpaceMaster) {
                    Context.Respond("You don't own this ship store and are not an admin");
                    return;
                }
            }

            // +++++ Find the grid +++++
            List<MyCubeGrid> grids = ALEUtils.GridFinder.FindGridList(null, player.Character, false);

            if (grids == null) {
                Context.Respond("Multiple grids found. Try to rename them first or try a different subgrid for identification!");
                return;
            }

            if (grids.Count == 0) {
                Context.Respond("No grids found. Check your viewing angle or try the correct name!");
                return;
            }

            // Make sure the Player calling this owns the ship (only applicable to non-admins)
            if (Context.Player.PromoteLevel < MyPromoteLevel.SpaceMaster) {
                foreach (MyCubeGrid _grid in grids) {
                    if (!_grid.BigOwners.Contains(Context.Player.IdentityId)) {
                        Context.Respond("You don't own enough of this grid to sell it -_-");
                        return;
                    }
                }
            }

            // Make sure that the grid isn't being sold already
            foreach (Config.RegisteredShipStore.RegisteredShipPresentToSell _ship in store.ShipsPresentToSell) {
                foreach (MyCubeGrid _grid in grids) {
                    if (_ship.ShipID == _grid.EntityId) {
                        Context.Respond("Cannot sell this grid, already being sold at this store");
                        return;
                    }
                }
            }

            // Create new shipToSell and add to ship store registry
            Config.RegisteredShipStore.RegisteredShipPresentToSell shipToSell = new Config.RegisteredShipStore.RegisteredShipPresentToSell();
            shipToSell.ShipName = grids[0].DisplayName;
            shipToSell.ShipID = grids[0].EntityId;
            shipToSell.ShipCost = _cost;

            Context.Respond($"Selling {grids[0].DisplayName} for {_cost} credits at {store.Name}");

            store.ShipsPresentToSell.Add(shipToSell);
        }

        [Command("listships", "Returns all ships that are available to buy at this ship store. Optionally, specifiy the store name")]
        [Permission(MyPromoteLevel.None)]
        public void GetAvailibleShips(string _storename = null) {
            Config.RegisteredShipStore store = null;

            // Get the current store since no store name was specified
            if (_storename == null) {
                if (Context.Player == null) {
                    Context.Respond("Could not find ship store - No player to get location from");
                    return;
                }

                store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);

                if (store == null) {
                    Context.Respond("Could not find a ship store at your present location");
                    return;
                }
            }

            // Try to get the store with the specified name (case insensitive)
            else {
                foreach (Config.RegisteredShipStore _store in Plugin.Config.RegisteredShipStores) {
                    if (_store.Name.ToLower() == _storename.ToLower()) {
                        store = _store;
                        break;
                    }
                }

                if (store == null) {
                    Context.Respond($"Could not find a ship store with the name {_storename}");
                    return;
                }
            }

            // We have the store! Lets generate a response string...
            string models = "Models: \n";
            foreach(Config.RegisteredShipStore.RegisteredShipModelToSell _model in store.ShipModelsToSell) {
                models += $"{_model.ShipName} - {_model.ShipCost} credits\n";
            }
            string presentShips = "Ships Present: \n";
            foreach (Config.RegisteredShipStore.RegisteredShipPresentToSell _ship in store.ShipsPresentToSell) {
                presentShips += $"{_ship.ShipName} - {_ship.ShipCost} credits | ID: {_ship.ShipID}\n";
            }

            // Respond with strings:
            Context.Respond($"Buyable ships at {store.Name}:");
            Context.Respond(models);
            Context.Respond(presentShips);
        }

        [Command("buyship", "Buy a ship from the ship store that you're looking at. Optionally, specify the blueprint name if the ship is a model and not present")]
        [Permission(MyPromoteLevel.None)]
        public void BuyShip(string _bpName = null) {
            Config.RegisteredShipStore store = null;

            // Can't do this if you are the server
            if (Context.Player == null) {
                Context.Respond("Could not find ship store - No player to get location from");
                return;
            }

            // Get the store from your current location
            store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);
            if (store == null) {
                Context.Respond("Could not find a ship store at your present location");
                return;
            }


            // Consumer is trying to buy a ship bp
            if (_bpName != null) {

                // Get the bp from the registry
                Config.RegisteredShipStore.RegisteredShipModelToSell model = null;
                foreach(Config.RegisteredShipStore.RegisteredShipModelToSell _model in store.ShipModelsToSell) {
                    if (_bpName.ToLower() == _model.ShipName.ToLower()) {
                        model = _model;
                    }
                }

                // Did we get it?!!!???!?!?!?
                if (model == null) {
                    Context.Respond($"{_bpName} not for sale");
                    return;
                }

                // Check if the player isn't too broke
                long playerAccountBalance = EconUtils.TryGetPlayerAccountBalance(Context.Player.IdentityId);
                if (playerAccountBalance < model.ShipCost) {
                    Context.Respond($"Sorry, you're too broke to buy this ship. You need {model.ShipCost} credits ^_^");
                    return;
                }

                // We got it!
                Context.Respond($"Buying {model.ShipName} for {model.ShipCost} credits");

                // Spawn in that bad boi
                ALEUtils.GridImportResult result = ALEUtils.GridManager.LoadGrid(Plugin.CreatePath(model.ShipName), Context.Player.GetPosition(), false, false, Context.Player.IdentityId);
                ALEUtils.GridImportResultWriter.WriteResult(Context, result);

                if (result == ALEUtils.GridImportResult.OK) {
                    EconUtils.ChangePlayerCreditAccount(Context.Player.IdentityId, -model.ShipCost);
                }
            }

            // Consumer is trying to buy a ship present in the store
            else {

                // Must be executed by a player
                if (Context.Player == null) {
                    Context.Respond("Cannot run this command as the server");
                    return;
                }

                // Must be executed by a character
                var player = ((MyPlayer)Context.Player).Identity;

                if (player.Character == null) {
                    Context.Respond("Player has no character");
                    return;
                }

                List<MyCubeGrid> grids = ALEUtils.GridFinder.FindGridList(null, player.Character, true);

                if (grids == null) {
                    Context.Respond("Multiple grids found. Try to rename them first or try a different subgrid for identification!");
                    return;
                }

                if (grids.Count == 0) {
                    Context.Respond("No grids found. Check your viewing angle or try the correct name!");
                    return;
                }

                // Try and find the ship data about the grids found
                Config.RegisteredShipStore.RegisteredShipPresentToSell ship = null;
                foreach (Config.RegisteredShipStore.RegisteredShipPresentToSell _ship in store.ShipsPresentToSell) {
                    foreach (MyCubeGrid grid in grids) {
                        // Check saved ids to be faster
                        if (grid.EntityId == _ship.ShipID) {
                            ship = _ship;
                        }
                    }
                }

                // Make sure we found the ship data we were looking for
                if (ship == null) {
                    Context.Respond("The grid you are looking at is not for sale");
                    return;
                }

                // Check if the player isn't too broke
                long playerAccountBalance = EconUtils.TryGetPlayerAccountBalance(Context.Player.IdentityId);
                if (playerAccountBalance < ship.ShipCost) {
                    Context.Respond($"Sorry, you're too broke to buy this ship. You need {ship.ShipCost} credits ^_^");
                    return;
                }

                // Make sure the faction selling this thing still exists
                if (MyVisualScriptLogicProvider.GetFactionMembers(store.OwnerFactionTag).Count == 0) {
                    Context.Respond($"Sorry, {store.OwnerFactionTag} that owns {store.Name} no longer exists");
                    return;
                }

                // Transfer Credits to and from faction / player
                EconUtils.ChangePlayerCreditAccount(Context.Player.IdentityId, -ship.ShipCost);
                EconUtils.ChangeFactionCreditAccount(MyVisualScriptLogicProvider.GetPlayersFactionTag(Context.Player.IdentityId), ship.ShipCost);

                // Transfer Ownership
                ShipStoreUtils.ClaimGrids(grids, Context.Player.IdentityId);

                // Remove listing from store
                store.ShipsPresentToSell.Remove(ship);

                Context.Respond($"You bought {ship.ShipName} for {ship.ShipCost} credits!!");
            }
        }

        [Command("removeshipsale", "Remove the sale listing from the current ship store")]
        [Permission(MyPromoteLevel.None)]
        public void RemoveShipFromStore() {
            Config.RegisteredShipStore store = null;

            // Must be executed by a player
            if (Context.Player == null) {
                Context.Respond("Cannot run this command as the server");
                return;
            }

            // Must be executed by a character
            var player = ((MyPlayer)Context.Player).Identity;

            if (player.Character == null) {
                Context.Respond("Player has no character");
                return;
            }

            // Get the store your character is at right now
            store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);
            if (store == null) {
                Context.Respond("Could not find a ship store at your present location");
                return;
            }

            // Make sure the Player calling this is in the same faction as the ship store or has space master permissions
            string playerFactionTag = MyVisualScriptLogicProvider.GetPlayersFactionTag(Context.Player.IdentityId);
            if (playerFactionTag != store.OwnerFactionTag) {
                if (Context.Player.PromoteLevel < MyPromoteLevel.SpaceMaster) {
                    Context.Respond("You don't own this ship store and are not an admin");
                    return;
                }
            }

            // Get the current grid you're looking at
            List<MyCubeGrid> grids = ALEUtils.GridFinder.FindGridList(null, player.Character, true);

            if (grids == null) {
                Context.Respond("Multiple grids found. Try to rename them first or try a different subgrid for identification!");
                return;
            }

            if (grids.Count == 0) {
                Context.Respond("No grids found. Check your viewing angle or try the correct name!");
                return;
            }

            // Try and find registered ship data in the grids found and try to remove the sale listing from the store
            foreach (Config.RegisteredShipStore.RegisteredShipPresentToSell _ship in store.ShipsPresentToSell) {
                foreach (MyCubeGrid grid in grids) {
                    // Check saved ids to be faster
                    if (grid.EntityId == _ship.ShipID) {
                        store.ShipsPresentToSell.Remove(_ship);
                        Context.Respond($"Removed {_ship.ShipName} from {store.Name}");
                        return;
                    }
                }
            }

            Context.Respond($"Grid was not for sale");
        }

        [Command("removeshipblueprintsale", "Remove the blueprint sale listing from the current ship store")]
        [Permission(MyPromoteLevel.SpaceMaster)]
        public void RemoveBlueprintFromStore(string _shipBp, string _storename = null) {
            Config.RegisteredShipStore store = null;

            // Get the current store since no store name was specified
            if (_storename == null) {
                if (Context.Player == null) {
                    Context.Respond("Could not find ship store - No player to get location from");
                    return;
                }

                store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);

                if (store == null) {
                    Context.Respond("Could not find a ship store at your present location");
                    return;
                }
            }

            // Try to get the store with the specified name (case insensitive)
            else {
                foreach (Config.RegisteredShipStore _store in Plugin.Config.RegisteredShipStores) {
                    if (_store.Name.ToLower() == _storename.ToLower()) {
                        store = _store;
                        break;
                    }
                }

                if (store == null) {
                    Context.Respond($"Could not find a ship store with the name {_storename}");
                    return;
                }
            }

            // Try and find the model to remove and remove it
            foreach (Config.RegisteredShipStore.RegisteredShipModelToSell _model in store.ShipModelsToSell) {
                if (_model.ShipName.ToLower() == _shipBp.ToLower()) {
                    store.ShipModelsToSell.Remove(_model);
                    Context.Respond($"Found and removed {_shipBp} from {store.Name}");
                    return;
                }
            }

            Context.Respond($"Could not find {_shipBp} in {store.Name}");
        }

        [Command("save", "saves the current ship store config")]
        [Permission(MyPromoteLevel.SpaceMaster)]
        public void SaveConfig() {
            Plugin.SaveConfig();
        }
        
        [Command("info", "Gets info about your current ship store and the ship you are looking at")]
        public void GetInfo() {
            Config.RegisteredShipStore store = null;
            string infoString = "";

            // Get the current store since no store name was specified
            if (Context.Player == null) {
                Context.Respond("Cannot run this command as the server -_-");
                return;
            }

            // Get Store Info
            store = Config.RegisteredShipStore.TryGetShipStoreFromLocation(Context.Player.GetPosition(), Plugin.Config.RegisteredShipStores);
            if (store == null) {
                infoString += $"Not in a ship store right now. ";
            }
            else {
                infoString += $"You are currently at {store.Name} which is owned by {store.OwnerFactionTag}. ";
            }

            // Get Grid Info

            var player = ((MyPlayer)Context.Player).Identity;

            if (player.Character == null) {
                infoString += "Player has no character. ";
            } else {
                List<MyCubeGrid> grids = ALEUtils.GridFinder.FindGridList(null, player.Character, true);

                if (grids.Count == 0) {
                    infoString += "No grids found. ";
                    Context.Respond(infoString);
                    return;
                }

                if (grids == null) {
                    infoString += "Multiple grids found. Try to rename them first or try a different subgrid for identification! ";
                }
                

                // Try and find the ship data about the grids found
                Config.RegisteredShipStore.RegisteredShipPresentToSell ship = null;
                foreach (Config.RegisteredShipStore.RegisteredShipPresentToSell _ship in store.ShipsPresentToSell) {
                    foreach (MyCubeGrid grid in grids) {
                        // Check saved ids to be faster
                        if (grid.EntityId == _ship.ShipID) {
                            ship = _ship;
                        }
                    }
                }

                if (ship == null) {
                    infoString += $"The grid you're looking at is not for sale. ";
                } else {
                    infoString += $"{ship.ShipName} is for sale for {ship.ShipCost} credits. This grid's id is {ship.ShipID}. ";
                }
            }

            Context.Respond(infoString);
        }
    }
}
