The ship store plugin allows you to sell prefab ships from ALE exporter files and pre-existing ships within defined zones called 'ship stores'.

# Availible Commands #

 - `!sstore createshipstore <string _storeName> [string _factionTag]` Creates a store at your current position with a specified name and faction. If no faction is specified, whoever calls this command's faction tag is used
 - `!sstore sellshipblueprint <string _shipBpName> <long _cost> [string _storename]` Adds a saved blueprint (Save ships using the ALE exporter's `!exportgrid <string filename>` command) to sell at the store you are presently located at. Cost in keen credits must be specified. Storename can be optionally specified to do things remotely
 - `!sstore sellship <long _cost>` Takes the grid and its subgrids you are looking at and puts it for sale at the current ship store at a specified cost in keen credits
 - `!sstore listships` Lists the ships for sale at your local ship shop
 - `!sstore buyship [string _bpName]` Buy the ship you are currently looking at. If you specify a blueprint name, tries to buy and spawn in the requested blueprint near you.
 - `!sstore removeshipsale ` Removes the ship you are looking at from the store sale listing so you can't buy it anymore
 - `!sstore removeshipblueprintsale <string _shipBpName> [string _storename]` Removes the ship blueprint with specified name from the store sale listing so you can't buy it anymore
 - `!sstore save` Saves the current config for reasons beyond my cat Maki Maki
 - `!sstore info` Displays info about the grid you are looking at