﻿using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VRage.ModAPI;
using VRage.Game.ModAPI;
using VRage.Game.Entity;
using VRageMath;
using Sandbox.Game.Entities;

namespace SOI_Conquest.Core {
    public class SOI {
        public Vector3D Center;
        public double Radius;
        public string Name;
        public long RevenuePerTick;

        public List<long> PastPlayerIDs; // List of all player ids that were in this SOI last update

        public List<long> OwningPlayerIDs; // List of all player ids currently controlling this SOI
        public long TreasurerPlayerID; // Player id the treasurer of this SOI

        public SOI () {
            PastPlayerIDs = new List<long>();
            OwningPlayerIDs = new List<long>();

            TreasurerPlayerID = -1L;
            Center = Vector3D.Zero;
            Radius = 0;
            Name = "My Soi";
            RevenuePerTick = 0L;
        }

        public SOI(string _name, Vector3D _center, double _radius) {
            PastPlayerIDs = new List<long>();
            OwningPlayerIDs = new List<long>();

            TreasurerPlayerID = -1L;
            Center = _center;
            Radius = _radius;
            Name = _name;
            RevenuePerTick = 0L;
        }

        /// <summary>
        /// Checks if a point in inside the SOI
        /// </summary>
        public bool ContainsPoint(Vector3D _point) {
            return Radius >= (_point - Center).Length();
        }

        /// <summary>
        /// Checks if a player is inside the SOI
        /// </summary>
        public bool ContainsPlayer(IMyPlayer _player) {
            return this.ContainsPoint(_player.GetPosition());
        }

        public List<MyEntity> GetEntitiesInSOI() {
            BoundingSphereD boundingSphere = new BoundingSphereD(Center, Radius);

            return MyEntities.GetTopMostEntitiesInSphere(ref boundingSphere);
        }
    }
}
