﻿using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using Torch.API;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.ModAPI;

namespace SOI_Conquest.Core {
    public class ServerPlayerRegistry {

        List<ServerPlayer> RegisteredPlayers;

        public ServerPlayerRegistry() {
            RegisteredPlayers = new List<ServerPlayer>();
        }
    }

    public class ServerPlayer {
        public IMyPlayer Player;
        public IMyFaction Faction;

        public List<SOI> LastKnownSOIs;

        public ServerPlayer(IMyPlayer _player) {
            Faction = MyAPIGateway.Session.Factions.TryGetPlayerFaction(_player.IdentityId);

            // Initialize SOIs as an empty list
            LastKnownSOIs = new List<SOI>();

            // To get all players:
            // MyAPIGateway.Multiplayer.Players;
        }
    }
}
