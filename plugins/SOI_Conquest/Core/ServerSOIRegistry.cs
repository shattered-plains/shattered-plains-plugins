﻿using Sandbox.ModAPI;
using SOI_Conquest.Config;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using Torch.API;
using VRage;
using VRageMath;
using VRage.Game.ModAPI;
using System.Runtime.Remoting.Contexts;
using Torch.API.Managers;
using Sandbox.Game;
using EmptyKeys.UserInterface.Generated.PlayerTradeView_Gamepad_Bindings;

namespace SOI_Conquest.Core {
    public class ServerSOIRegistry {
        public List<SOI> RegisteredSOIs;

        public ServerSOIRegistry(ObservableCollection<Config.RegisteredSOI> _registeredSois) {
            // Register all SOIs
            RegisteredSOIs = new List<SOI>();

            // Get all SOIs from config and import properties
            foreach(Config.RegisteredSOI _regSoi in _registeredSois) {
                SOI newSOI = new SOI(
                        _regSoi.Name,
                        new Vector3D(_regSoi.X, _regSoi.Y, _regSoi.Z),
                        _regSoi.Radius);

                // Add owning player ids
                foreach (Config.RegisteredSOI.SOIPlayer _soiPlayer in _regSoi.SOIPlayers) {
                    newSOI.OwningPlayerIDs.Add(_soiPlayer.PlayerID);

                    // Check if the player is the treasurer
                    if (_soiPlayer.IsTreasurer) {
                        newSOI.TreasurerPlayerID = _soiPlayer.PlayerID;
                    }
                }

                // Add new SOI to list of registered SOIs
                RegisteredSOIs.Add(newSOI);
            }
        }

        /// <summary>
        /// Copies current contents of all registered SOIs to the SOI Configuration classes
        /// </summary>
        public void CopySOIRegistryToConfigClasses(ref SOIConfig _configData) {
            
        }

        /// <summary>
        /// Tries to get the registered soi with the specified name. If none are found, returns null;
        /// </summary>
        public SOI TryGetSOIWithName(string _name) {
            _name = _name.ToLower().Trim();
            foreach (SOI _soi in RegisteredSOIs) {
                if (_soi.Name.ToLower().Equals(_name)) {
                    return _soi;
                }
            }

            return null; // Didn't find anything :(
        }

        public void RegisterSOI(SOI _soi) {
            RegisteredSOIs.Add(_soi);
        }

        public void CreateAndRegisterSOIWithName(string _name) {
            SOI _newSOI = new SOI();
            _newSOI.Name = _name;
            RegisteredSOIs.Add(_newSOI);
        }

        /// <summary>
        /// Returns true if removal of specified SOI was successful
        /// </summary>
        public bool UnRegisterSOI(SOI _soi) {
            return RegisteredSOIs.Remove(_soi);
        }

        /// <summary>
        /// Checks if an soi with the specified name already exists
        /// </summary>
        public bool SOIWithNameExists(string _name) {
            foreach (SOI _soi in RegisteredSOIs) {
                if (_soi.Name == _name) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Searches registered sois for an soi with the specified name. Will return true if it finds an soi
        /// </summary>
        public bool UnRegisterSOIWithName(string _name) {
            foreach (SOI _soi in RegisteredSOIs) {
                if (_soi.Name == _name) {
                    UnRegisterSOI(_soi);
                    return true;
                }
            }
            return false; // Couldn't find soi with name
        }

        public void UpdateSOIPlayers() {
            List<IMyPlayer> _players = new List<IMyPlayer>();
            MyAPIGateway.Multiplayer.Players.GetPlayers(_players); // Get all players

            // Check SOIs for players entering and leaving
            foreach(SOI _soi in RegisteredSOIs) {
                foreach(IMyPlayer _player in _players) {
                    bool wasInSOI = _soi.PastPlayerIDs.Contains(_player.IdentityId);
                    bool isInSOI = _soi.ContainsPlayer(_player);

                    // ^ is the C# XOR operator
                    if (wasInSOI ^ isInSOI) {
                        if (isInSOI) { // Entered SOI
                            HandleEnteringSOI(_player, _soi);

                            _soi.PastPlayerIDs.Add(_player.IdentityId); // Register player as in SOI
                        }
                        else { // Left SOI
                            HandleLeavingSOI(_player, _soi);

                            _soi.PastPlayerIDs.Remove(_player.IdentityId); // Unregister player from SOI
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Code that is called when a player leaves a SOI
        /// </summary>
        /// <param name="_player"></param>
        /// <param name="_soi"></param>
        public void HandleLeavingSOI(IMyPlayer _player, SOI _soi) {
            SOIPlugin.Log.Info($"Player {_player.DisplayName} left the soi {_soi.Name}");

            // Context.Torch.CurrentSession?.Managers?.GetManager<IChatManagerServer>?
            string message = $"Leaving {_soi.Name}";

            // MyVisualScriptLogicProvider.SendChatMessage(message, "Server", _player.IdentityId);
            MyVisualScriptLogicProvider.ShowNotification(message, 5000, "White", _player.IdentityId);

        }

        /// <summary>
        /// Code that is called when a player enters a SOI
        /// </summary>
        /// <param name="_player"></param>
        /// <param name="_soi"></param>
        public void HandleEnteringSOI(IMyPlayer _player, SOI _soi) {
            SOIPlugin.Log.Info($"Player {_player.DisplayName} entered the soi {_soi.Name}");

            string message = $"Entering {_soi.Name}";

            // MyVisualScriptLogicProvider.SendChatMessage(message, "Server", _player.IdentityId);
            MyVisualScriptLogicProvider.ShowNotification(message, 5000, "White", _player.IdentityId);
        }
    }
}
