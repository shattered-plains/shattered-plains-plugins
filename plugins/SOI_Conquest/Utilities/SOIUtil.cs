﻿using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.ModAPI;
using SOI_Conquest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.ModAPI;
using VRage.Game;
using VRage.Game.Entity;
using VRage.ObjectBuilders;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.Entities.Cube;
using VRage.Game.ModAPI;

namespace SOI_Conquest.Utilities {
    public struct WeightedFaction {
        public string FactionTag;
        public double Weight;

        public WeightedFaction(string _factionTag, double _weight) {
            FactionTag = _factionTag;
            Weight = _weight;
        }

        public List<long> GetPlayers() {
            return MyVisualScriptLogicProvider.GetFactionMembers(FactionTag);
        }
    }

    public class SOIUtil {

        public static List<WeightedFaction> GetWeightedFactionsInSOI(SOI _soi) {
            List<WeightedFaction> _factions = new List<WeightedFaction>();

            // Get all static grids in the SOI
            List<MyEntity> _entities = _soi.GetEntitiesInSOI();

            SOIPlugin.Log.Info($"Found {_entities.Count} in soi");

            for (int i = 0; i < _entities.Count; i++) {
                // 'as' cast will return a null value if cannot cast, so check for that
                var _entity = _entities[i] as MyCubeGrid;
                if (_entity == null) continue; // Ignore non-block grids (voxels, mobs, etc...)
                if (!_entity.IsStatic) continue; // Ignore non-static grids
                if (_entity.GridSizeEnum == MyCubeSize.Small) continue; // Ignore small grids

                SOIPlugin.Log.Info($"Found entity {_entity.DisplayName} with {_entity.BlocksCount} blocks, {_entity.BlocksPCU} PCU, and {_entity.NumberOfReactors} reactors");

                SOIPlugin.Log.Info(_entity.BlocksCounters.ToString());
                foreach (var key in _entity.BlocksCounters.Keys) {
                    SOIPlugin.Log.Info($"{key.ToString()}, {_entity.BlocksCounters[key].ToString()}");
                }
                
                // Get all beacon and radio antenna blocks
                IMyCubeGrid _grid = _entity as MyCubeGrid;
                var blocks = new List<IMySlimBlock>();
                _grid.GetBlocks(blocks, block => block.FatBlock != null && block.FatBlock is IMyFunctionalBlock
                        && (block.FatBlock.BlockDefinition.TypeId == typeof(MyObjectBuilder_Beacon) ||
                            block.FatBlock.BlockDefinition.TypeId == typeof(MyObjectBuilder_RadioAntenna)));

                // Get all enabled beacon and radio antenna blocks as IMyFunctionBlock
                var commsBlocks = blocks.Select(block => (IMyFunctionalBlock)block.FatBlock).Where(block => block.Enabled).ToArray();
                foreach (var commsBlock in commsBlocks) {
                    IMyBeacon _beacon = commsBlock as IMyBeacon;
                    IMyRadioAntenna _radio = commsBlock as IMyRadioAntenna;

                    if (_beacon != null) {
                        SOIPlugin.Log.Info($"Found enabled beacon with name {_beacon.CustomName} and radius {_beacon.Radius}");
                    }
                    else if (_radio != null) {
                        SOIPlugin.Log.Info($"Found enabled antenna with name {_radio.CustomName} and radius {_radio.Radius}");
                    }
                    else {
                        SOIPlugin.Log.Warn($"Could not identify block {commsBlock.CustomName} as a antenna or beacon");
                    }
                }
            }
            // MyObjectBuilder_RadioAntenna;
            return _factions;
        }

    }
}
