﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game.VisualScripting;
using Sandbox;
using VRage;
using Sandbox.ModAPI;

namespace SOI_Conquest.Utilities {
    public class EconUtil {

        /// <summary>
        /// Attempts to add an amount to an account associated with a playerID
        /// </summary>
        public static void AddAmountToPlayerAccount(long _playerId, long _amount) {
            // Implement a small try catch in-case this wants to throw a "can't find player exception"
            try {
                MyAPIGateway.Multiplayer.Players.RequestChangeBalance(_playerId, _amount);
            } catch (Exception e) {
                SOIPlugin.Log.Warn(e.Message);
            }
        }

        
    }
}
