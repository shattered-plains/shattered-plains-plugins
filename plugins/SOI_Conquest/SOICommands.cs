﻿using NLog.Fluent;
using SOI_Conquest.Core;
using SOI_Conquest.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;
using VRageMath;

namespace SOI_Conquest {

    [Category("soi")]
    public class SOICommands : CommandModule {

        // Get our current plugin instance from Context.Plugin
        public SOIPlugin Plugin => (SOIPlugin)Context.Plugin;


        [Command("createsoi", "Creates a new SOI with a specified name]")]
        [Permission(MyPromoteLevel.Admin)]
        public void CreateSOI(string _soiName) {
            Plugin.SOIRegistry.CreateAndRegisterSOIWithName(_soiName.Trim());
        }

        [Command("removesoi", "Removes a SOI with a specified name]")]
        [Permission(MyPromoteLevel.Admin)]
        public void RemoveSOI(string _soiName) {
            if (Plugin.SOIRegistry.UnRegisterSOIWithName(_soiName.Trim().ToLower())) {
                // Successfully removed SOI
                Context.Respond($"Successfully removed SOI with name {_soiName}");
            }
            else {
                // Couldn't find the soi :(
                Context.Respond($"Could not find SOI with name {_soiName}");
            }
        }


        /// <summary>
        /// Should be in format !soi get "My SOI Name" <property>. Is accessable by anyone
        /// </summary>
        [Command("get", "Gets a property of the specified SOI")]
        [Permission(MyPromoteLevel.None)]
        public void GetProperty(string _soiName, string _property) {
            SOI _soi = Plugin.SOIRegistry.TryGetSOIWithName(_soiName);

            // Couldn't find the SOI
            if (_soi == null) {
                Context.Respond($"Couldn't find a SOI with name {_soiName}");
                return;
            }

            string responseString;

            // Clean up input by converting all to lower and trimming whitespace
            _property = _property.ToLower().Trim();

            // Give back the appropiate information
            switch (_property) {
                case "center":
                    responseString = _soi.Center.ToString("0");
                    break;
                case "radius":
                    responseString = _soi.Radius.ToString("0");
                    break;
                case "revenue":
                    responseString = _soi.RevenuePerTick.ToString();
                    break;
                case "name":
                    responseString = _soi.Name;
                    break;
                default:
                    Context.Respond($"Couldn't find property '{_property}' of the SOI");
                    return;
            }

            // Respond to the player that asked
            Context.Respond($"The {_property} of soi '{_soiName}' is {responseString}");
        }

        /// <summary>
        /// Should be in format !soi set "My SOI Name" <property> <value>
        /// </summary>
        [Command("set", "Sets a property of the specified SOI")]
        [Permission(MyPromoteLevel.None)]
        public void SetProperty(string _soiName, string _property) {
            SOI _soi = Plugin.SOIRegistry.TryGetSOIWithName(_soiName);

            // Couldn't find the SOI
            if (_soi == null) {
                Context.Respond($"Couldn't find a SOI with name {_soiName}");
                return;
            }

            // Clean up input by converting all to lower and trimming whitespace
            _property = _property.ToLower().Trim();

            string responseString;

            // Give back the appropiate information
            try {
                switch (_property) {
                    case "center":
                        // Make sure we have three arguments
                        if (Context.Args.Count < 5) {
                            Context.Respond("Insufficent arguments to build a vector");
                            return;
                        }

                        _soi.Center = new Vector3D(double.Parse(Context.Args[2]),
                                            double.Parse(Context.Args[3]),
                                            double.Parse(Context.Args[4]));

                        responseString = _soi.Center.ToString("0");
                        break;
                    case "radius":
                        _soi.Radius = double.Parse(Context.Args[2]);
                        responseString = _soi.Radius.ToString("0");
                        break;
                    case "revenue":
                        _soi.RevenuePerTick = long.Parse(Context.Args[2]);
                        responseString = _soi.RevenuePerTick.ToString();
                        break;
                    case "name":
                        _soi.Name = Context.Args[2].Trim();
                        responseString = _soi.Name;
                        break;
                    default:
                        Context.Respond($"Couldn't find property '{_property}' of the SOI");
                        return;
                }
            } catch (Exception e) {
                Context.Respond("Could not understand your data -_-");
                return;
            }

            Context.Respond($"Set property {_property} of {_soiName} to {responseString}");
        }


        [Command("listsois", "Displays a chat message with a list off all currently registered SOIs")]
        [Permission(MyPromoteLevel.None)]
        public void ListSOIs() {
            string responseString = "";
            foreach(SOI _soi in Plugin.SOIRegistry.RegisteredSOIs) {
                responseString += $"\"{_soi.Name}\" ";
            }
            Context.Respond(responseString);
        }

        #region Commands for interfacing with the Essentials' auto-command scheduler

        /// <summary>
        /// This command should run every time SOIs are auto-updated based on player movements and the like
        /// Put revenue, SOI control stuff, and other things here
        /// </summary>
        [Command("runsoiupdate", "Runs an SOI update")]
        [Permission(MyPromoteLevel.Admin)]
        public void RunSOIUpdate() {
            SOIPlugin.Log.Info("Ran an SOI update");
        }

        [Command("testsoiweight", "  ")]
        [Permission(MyPromoteLevel.Admin)]
        public void TestSOIWeight(string _soiName) {
            SOI _soi = Plugin.SOIRegistry.TryGetSOIWithName(_soiName);

            // Couldn't find the SOI
            if (_soi == null) {
                Context.Respond($"Couldn't find a SOI with name {_soiName}");
                return;
            }

            SOIUtil.GetWeightedFactionsInSOI(_soi);
        }


        #endregion
    }
}
