﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using Microsoft.Xml.Serialization.GeneratedAssembly;

using NLog;
using SOI_Conquest.Core;
using Torch;
using Torch.API;
using Torch.API.Session;
using Torch.API.Managers;
using Torch.Session;
using Sandbox.ModAPI;

namespace SOI_Conquest {
    public class SOIPlugin : TorchPluginBase {

        private Persistent<SOIConfig> _config;
        public SOIConfig Config => _config?.Data;

        public ServerSOIRegistry SOIRegistry;

        private TorchSessionManager _sessionManager;

        public static readonly Logger Log = LogManager.GetLogger("SOI");

        private Timer _soiUpdateTimer;

        public override void Init(ITorchBase _torch) {
            base.Init(_torch);

            // Run configuration setup
            SetupConfig();

            // Register method 'SessionChanged' to SessionStateChanged action
            _sessionManager = Torch.Managers.GetManager<TorchSessionManager>();
            if (_sessionManager != null) {
                _sessionManager.SessionStateChanged += SessionChanged; // Register function to event
            }
            else {
                Log.Warn("No session manager loaded");
            }

            // Create SOI Registry
            SOIRegistry = new ServerSOIRegistry(Config.RegisteredSOIs);

            // Run a private function to log some information to the log so we know its working
            PrintoutStartupInfo();
        }

        /// <summary>
        /// Registers Timers with Specified delay settings
        /// </summary>
        private void RegisterTimers() {
            // Make sure that we aren't trying to run this with a delay of 0
            if (Config.SecondsPerSOIUpdate <= 0) {
                Log.Warn($"Could not initialize SOI update timer with a delay time of {Config.SecondsPerSOIUpdate}\n" +
                    $"Setting config property 'SecondsPerSOIUpdate' to default value of 1 second");
                _soiUpdateTimer = new Timer(1000);
            } else {
                _soiUpdateTimer = new Timer(Config.SecondsPerSOIUpdate * 1000); // Mult by 1000 to convert to ms from seconds
            }

            _soiUpdateTimer.AutoReset = true; // Repeat timer
            _soiUpdateTimer.Elapsed += UpdateSOIs; // Add a function to the Elapsed event on the timer (called whenever the delay time has elapsed)
            _soiUpdateTimer.Start(); // Start the timer
        }

        private void UnregisterTimers() {
            _soiUpdateTimer.Elapsed -= UpdateSOIs; // Unregister the function from the timer to stop execution
            _soiUpdateTimer.Stop(); // Stop the timer
        }

        /// <summary>
        /// Main update cycle. Should update players in all sois (_source and _e are not used but need to be included for this to work)
        /// </summary>
        public void UpdateSOIs(object _source, ElapsedEventArgs _e) {
            SOIRegistry.UpdateSOIPlayers();
        }

        /// <summary>
        /// Called every time the session state is changed (ie. the session is now unloading)
        /// </summary>
        private void SessionChanged(ITorchSession _session, TorchSessionState _state) {
            switch (_state) {
                case TorchSessionState.Loaded:
                    Log.Info("Torch Session Loaded");

                    RegisterTimers();
                    break;
                case TorchSessionState.Unloading:
                    Log.Info("Torch Session Unloading");

                    SaveConfiguration();
                    UnregisterTimers();
                    break;
            }
        }

        /// <summary>
        /// Saves currently loading SOI and plugin configurations to the config file
        /// </summary>
        private void SaveConfiguration() {
            // Put all current sois into the config file
            foreach (SOI _soi in SOIRegistry.RegisteredSOIs) {
                // Search for the soi with a matching name
                bool foundSoi = false;
                for (int i = 0; i < Config.RegisteredSOIs.Count; i++) {
                    // Do some processing to trim whitespace and ignore case
                    if (Config.RegisteredSOIs[i].Name.Trim().ToLower() == _soi.Name.Trim().ToLower()) {
                        foundSoi = true;

                        // Update properties of the soi
                        Config.RegisteredSOIs[i].Radius = _soi.Radius;
                        Config.RegisteredSOIs[i].Revenue = _soi.RevenuePerTick;
                        Config.RegisteredSOIs[i].X = _soi.Center.X;
                        Config.RegisteredSOIs[i].Y = _soi.Center.Y;
                        Config.RegisteredSOIs[i].Z = _soi.Center.Z;

                        // Update list of owning players too
                        Config.RegisteredSOIs[i].SOIPlayers.Clear(); // Delete all currently registered players
                        foreach (long _playerID in _soi.OwningPlayerIDs) {
                            Config.RegisteredSOI.SOIPlayer _soiPlayer = new Config.RegisteredSOI.SOIPlayer();
                            _soiPlayer.PlayerID = _playerID;
                            Config.RegisteredSOIs[i].SOIPlayers.Add(_soiPlayer);
                        }
                    }
                }

                // If we didn't find a SOI in the config file with a matching name, name a new one
                if (!foundSoi) {
                    Config.RegisteredSOI _rSoi = new Config.RegisteredSOI();
                    _rSoi.Name = _soi.Name;
                    _rSoi.Radius = _soi.Radius;
                    _rSoi.X = _soi.Center.X;
                    _rSoi.Y = _soi.Center.Y;
                    _rSoi.Z = _soi.Center.Z;
                    _rSoi.Revenue = _soi.RevenuePerTick;

                    // Add new soi to list of sois
                    Config.RegisteredSOIs.Add(_rSoi);
                }
            }

            // Save current configuration to file
            _config.Save();
        }

        /// <summary>
        /// Sets up config file reading and attempts to read from existing config file into Config classes
        /// </summary>
        private void SetupConfig() {
            var configFile = Path.Combine(StoragePath, "SOIConfig.cfg");

            try {
                // Try to load configuration from existing file
                _config = Persistent<SOIConfig>.Load(configFile);
            } catch (Exception e) {
                Log.Warn(e);
            }

            // Handle situation where there is no existing 'SOIConfig.cfg'
            if (_config?.Data == null) {
                Log.Info($"No existing configuration found. Creating new config '{configFile}' at path: {StoragePath}");

                _config = new Persistent<SOIConfig>(configFile, new SOIConfig());
                _config.Save();
            }
        }

        private void PrintoutStartupInfo() {
            // Print out number of registered SOIs
            Log.Info($"Getting {Config.RegisteredSOIs.Count} Registered SOIs");
            // Lets try printing out all SOI names
            foreach (Config.RegisteredSOI _soi in Config.RegisteredSOIs) {
                Log.Info($"Registered SOI with Name {_soi.Name}");
            }
        }
    }
}
