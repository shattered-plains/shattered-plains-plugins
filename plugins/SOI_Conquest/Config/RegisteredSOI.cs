﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using VRage;
using VRage.Collections;
using VRage.Game.ModAPI;
using VRageMath;

namespace SOI_Conquest.Config {
    public class RegisteredSOI : ViewModel {

        public RegisteredSOI() {
            SOIPlayers.CollectionChanged += (sender, args) => OnPropertyChanged();
        }

        private string _name;
        private long _revenue;

        private double _x;
        private double _y;
        private double _z;
        private double _radius;


        public string Name { get { return _name; } set { _name = value; } }

        public double Radius { get { return _radius; } set { _radius = value; } }

        public double X { get => _x; set => SetValue(ref _x, value); }
        public double Y { get => _y; set => SetValue(ref _y, value); }
        public double Z { get => _z; set => SetValue(ref _z, value); }

        public long Revenue { get { return _revenue; } set { _revenue = value; } }

        public ObservableCollection<SOIPlayer> SOIPlayers { get; } = new ObservableCollection<SOIPlayer>();


        public class SOIPlayer : ViewModel {
            private long _playerid;
            private bool _istreasurer;

             
            public long PlayerID { get => _playerid; set => SetValue(ref _playerid, value); }
            public bool IsTreasurer { get => _istreasurer; set => SetValue(ref _istreasurer, value); }

            public override string ToString() {
                return $"{_playerid} : isTreasurer={_istreasurer}";
            }
        }

        public override string ToString() {
            return $"{this._name}";
        }
    }
}
