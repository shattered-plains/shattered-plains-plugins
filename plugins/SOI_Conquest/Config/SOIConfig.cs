﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using Torch.Views;

using SOI_Conquest.Core;
using System.Xml.Serialization;

namespace SOI_Conquest {
    public class SOIConfig : ViewModel {

        public SOIConfig() {
            RegisteredSOIs.CollectionChanged += (sender, args) => OnPropertyChanged();
        }

        /// <summary>
        /// A collection of all registered SOIs
        /// </summary>
        public ObservableCollection<Config.RegisteredSOI> RegisteredSOIs { get; } = new ObservableCollection<Config.RegisteredSOI>();

        /// <summary>
        /// How many seconds to wait between updating which players are in which SOI
        /// </summary>
        private double _secondsPerSOIUpdate;
        public double SecondsPerSOIUpdate { get => _secondsPerSOIUpdate; set => SetValue(ref _secondsPerSOIUpdate, value); }
    }
}
