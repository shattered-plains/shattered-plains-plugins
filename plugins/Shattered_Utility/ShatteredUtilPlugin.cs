﻿using NLog;
using Shattered_Utility.Config;
using Shattered_Utility.Core;
using System;
using System.IO;
using System.Timers;
using System.Windows;
using Torch;
using Torch.API;
using Torch.API.Managers;
using Torch.API.Session;
using Torch.Session;

namespace ShatteredUtility {
    public class ShatteredUtilPlugin : TorchPluginBase {

        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private Persistent<ShatteredUtilConfig> _config;
        public ShatteredUtilConfig Config => _config?.Data;

        private TorchSessionManager _sessionManager;

        private Timer _oneSecondTimer;

        public PlayerCommandDelayTracker ClaimDelayTracker;

        public override void Init (ITorchBase torch) {
            base.Init(torch);

            SetupConfig();

            // Register method 'SessionChanged' to SessionStateChanged action
            _sessionManager = Torch.Managers.GetManager<TorchSessionManager>();
            if (_sessionManager != null) {
                _sessionManager.SessionStateChanged += SessionChanged; // Register function to event
            } else {
                Log.Warn("No session manager loaded");
            }

            // Setup Claim Delay Tracker
            ClaimDelayTracker = new PlayerCommandDelayTracker();
            ClaimDelayTracker.TotalDelayTime = Config.MinDelayBetweenClaims;
        }

        private void SetupConfig() {

            string configFile = Path.Combine(StoragePath, "Shattered_Utility.cfg");

            try {

                _config = Persistent<ShatteredUtilConfig>.Load(configFile);

            } catch (Exception e) {
                Log.Warn(e);
            }

            if (_config?.Data == null) {

                Log.Info("Create Default Config, because none was found!");

                _config = new Persistent<ShatteredUtilConfig>(configFile, new ShatteredUtilConfig());
                _config.Save();
            }
        }

        /// <summary>
        /// Called every time the session state is changed (ie. the session is now unloading)
        /// </summary>
        private void SessionChanged(ITorchSession _session, TorchSessionState _state) {
            switch (_state) {
                case TorchSessionState.Loaded:
                    Log.Info("Torch Session Loaded");

                    RegisterTimers();
                    break;
                case TorchSessionState.Unloading:
                    Log.Info("Torch Session Unloading");

                    UnregisterTimers();
                    break;
            }
        }

        /// <summary>
        /// Registers timers
        /// </summary>
        private void RegisterTimers() {
            // Register timer to update once per second
            _oneSecondTimer = new Timer(1000);

            _oneSecondTimer.AutoReset = true; // Repeat timer
            _oneSecondTimer.Elapsed += UpdatePerOneSecond; // Add a function to the Elapsed event on the timer (called whenever the delay time has elapsed)
            _oneSecondTimer.Start(); // Start the timer
        }

        private void UnregisterTimers() {
            _oneSecondTimer.Elapsed -= UpdatePerOneSecond;
            _oneSecondTimer.Stop();
        }

        /// <summary>
        /// Function to be called once per second
        /// </summary>
        public void UpdatePerOneSecond(object _source, ElapsedEventArgs _e) {
            ClaimDelayTracker.TickDownPlayerTimes(1);
        }
    }
}
