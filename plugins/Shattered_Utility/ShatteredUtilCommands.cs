﻿using NLog.Fluent;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Cube;
using Sandbox.Game.World;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;

namespace ShatteredUtility {
    public class ShatteredUtilCommands : CommandModule {
        public ShatteredUtilPlugin Plugin => (ShatteredUtilPlugin)Context.Plugin;

        [Command("claim", "Attempts to claim the grid a player is currently seated on. Can only claim grid if all beacons belong to the player in question. If the grid claim fails, all unowned beacons are turned on and forced to have a config defined range")]
        [Permission(MyPromoteLevel.None)]
        public void ClaimGrid() {
            if (Context.Player is null) {
                Context.Respond("Cannot claim a grid for the server");
                return;
            }

            long _playerId = Context.Player.IdentityId;

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //           Check to make sure player can run command at the moment
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            int _playerDelayTime = Plugin.ClaimDelayTracker.GetDelayTimeForPlayer(_playerId);

            if (_playerDelayTime > 0) {
                Context.Respond($"Cannot run this command for {_playerDelayTime} seconds");
                return;
            }
            
            // Ok, lets actually attempt to run the command
            else {
                // Add the player to the delay tracker so they can't run the command for X time
                Plugin.ClaimDelayTracker.AddPlayer(_playerId);
            }

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //                           Get controlled Grid Data
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            MyVisualScriptLogicProvider.GetPlayerControlledBlockData(out string _controlType, 
                    out long _blockId, 
                    out string _blockName, 
                    out long _gridId, 
                    out string _gridName, 
                    out bool _isRespawnShip, 
                    _playerId);

            MyCubeGrid _entity = MyEntities.GetEntityById(_gridId) as MyCubeGrid;
            IMyCubeGrid _grid = _entity as MyCubeGrid;

            if (_entity is null) {
                Context.Respond("Must be seated on a grid to claim it");
                return;
            }

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //            Check if all beacons are owned and turn on unowned ones
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            bool _allBeaconsOwned = true;

            // Get all beacon blocks
            var blocks = new List<IMySlimBlock>();
            _grid.GetBlocks(blocks, block => block.FatBlock != null && block.FatBlock is IMyFunctionalBlock
                    && (block.FatBlock.BlockDefinition.TypeId == typeof(MyObjectBuilder_Beacon)));

            // Get all functional beacon blocks as IMyFunctionBlock types
            var commsBlocks = blocks.Select(block => (IMyFunctionalBlock)block.FatBlock).Where(block => block.IsFunctional).ToArray();
            foreach (var commsBlock in commsBlocks) {
                IMyBeacon _beacon = commsBlock as IMyBeacon;

                // Check to make sure block selected is actually a beacon
                if (_beacon is null) {
                    ShatteredUtilPlugin.Log.Warn($"Could not convert block {commsBlock.CustomName} to type IMyBeacon");
                    continue;
                }

                // Check if beacon is not owned
                if (_beacon.GetUserRelationToOwner(_playerId) != MyRelationsBetweenPlayerAndBlock.Owner) {
                    _allBeaconsOwned = false;

                    _beacon.SetValue("Radius", Plugin.Config.ClaimBeaconRange); // Have to use a terminal property because Keen doesn't let us touch it otherwise
                    _beacon.Enabled = true;
                }
            }

            // Log this call of claim to the log
            string logInfo = $"Player {Context.Player.DisplayName} called claim command on grid {_entity.DisplayName}.";
            logInfo += "The call was " + (_allBeaconsOwned ? "successful" : "unsuccessful") + " in claiming the grid";
            ShatteredUtilPlugin.Log.Info(logInfo);

            if (!_allBeaconsOwned) {
                Context.Respond($"You do not own all beacons on this grid. Cannot claim it cause you're a meanie. I'll set all the beacons you don't own to {Plugin.Config.ClaimBeaconRange.ToString("0")} meters tho, so don't get all mad or anything (^_^);");
                return;
            }

            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //                             Change Grid Ownership
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            _grid.ChangeGridOwnership(_playerId, MyOwnershipShareModeEnum.Faction);

            Context.Respond($"You are now the proud owner of {_entity.DisplayName}. Safe engineering!");
        }
    }
}
