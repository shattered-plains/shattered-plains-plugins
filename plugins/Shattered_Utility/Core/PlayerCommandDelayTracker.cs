﻿using Sandbox.Game;
using ShatteredUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shattered_Utility.Core {
    public class PlayerCommandDelayTracker {

        public int TotalDelayTime; // Seconds
        private List<Player> Players;

        public PlayerCommandDelayTracker() {
            Players = new List<Player>();
        }

        /// <summary>
        /// Should be called once a <_tickTime>. Ticks down each registered player's time until they are allowed to call their next command
        /// </summary>
        public void TickDownPlayerTimes(int _tickTime) {
            foreach(Player _player in Players) {
                _player.TimeUntilAllowed -= _tickTime;
                if (_player.TimeUntilAllowed <= 0) {
                    Players.Remove(_player);

                    // Send the player a log message to let them know
                    MyVisualScriptLogicProvider.SendChatMessage("You can now use the claim command again", "Server", _player.IdentityID);
                }
            }
        }

        /// <summary>
        /// Adds a Player to the Delay Tracker
        /// </summary>
        public void AddPlayer(long _identityID) {
            Player _newPlayer = new Player();
            _newPlayer.IdentityID = _identityID;
            _newPlayer.TimeUntilAllowed = TotalDelayTime;

            Players.Add(_newPlayer);
        }

        /// <summary>
        /// Attempts to get the remaining delay time for a player. Will return 0 if player is not found
        /// </summary>
        public int GetDelayTimeForPlayer(long _identityID) {
            foreach(Player _player in Players) {
                // Find the player with the identity id
                if (_player.IdentityID == _identityID) {
                    return _player.TimeUntilAllowed;
                }
            }

            // Could not find a player. Return 0
            return 0;
        }

        public class Player {
            public long IdentityID;
            public int TimeUntilAllowed; // Seconds

            public Player() {
                IdentityID = 0L;
                TimeUntilAllowed = 0;
            }
        }
    }
}
