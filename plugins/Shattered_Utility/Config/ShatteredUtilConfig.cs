﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torch;

namespace Shattered_Utility.Config {
    public class ShatteredUtilConfig : ViewModel {

        /// <summary>
        /// Default Constructor (put default values here)
        /// </summary>
        public ShatteredUtilConfig() {
            _claimBeaconRange = 100F;
            _minDelayBetweenClaims = 300; // 5 minutes
        }

        /// <summary>
        /// Range to set unowned beacons to whe attempting to claim a grid using the !claim command
        /// </summary>
        private float _claimBeaconRange;
        public float ClaimBeaconRange { get => _claimBeaconRange; set => SetValue(ref _claimBeaconRange, value); }

        /// <summary>
        /// The minimum delay between calls of claim for each player (seconds)
        /// </summary>
        private int _minDelayBetweenClaims;
        public int MinDelayBetweenClaims { get => _minDelayBetweenClaims; set => SetValue(ref _minDelayBetweenClaims, value); }
    }
}
