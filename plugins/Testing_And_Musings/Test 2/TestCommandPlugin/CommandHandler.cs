﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Torch;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;

namespace TestCommandPlugin {
    public class CommandHandler : CommandModule {

        [Command("echo", "This function echo's back the first argument provided")]
        [Permission(MyPromoteLevel.None)]
        public void Echo(string arg) {
            string whoCalled = (Context.Player is null) ? "The Server" : Context.Player.DisplayName;
            string output = $"{whoCalled} called the echo command with argument: {arg}";

            Context.Respond(output);

            Main.Log.Info(output);
        }

    }
}
