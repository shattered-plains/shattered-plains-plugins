﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Sandbox;
using Sandbox.Engine.Multiplayer;
using Sandbox.Game.World;
using Torch;
using Torch.API.Managers;
using Torch.Commands;
using Torch.Commands.Permissions;
using Torch.Managers.ChatManager;
using Torch.Mod;
using Torch.Mod.Messages;
using VRage.Game.ModAPI;
using VRageRender.Utils;

using System.Runtime;

namespace TestPlugin {
    public class CommandPluginClass : CommandModule {

        [Command("echo", "This command should echo back what you said to you")]
        [Permission(VRage.Game.ModAPI.MyPromoteLevel.None)]
        public void Echo(string input) {
            Context.Respond($"Recieved: {input}");
        }
    }
}
