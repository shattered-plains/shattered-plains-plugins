﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime;

using NLog;
using NLog.Fluent;
using Torch;
using Torch.API;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage.Game.ModAPI;

namespace TestPlugin {
    public class TestPlugin : TorchPluginBase {

        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public override void Init(ITorchBase torch) {
            base.Init(torch);

            Log.Info("This is a log from the Test Plugin");
        }
    }
}
